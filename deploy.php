<?php
namespace Deployer;

require 'recipe/rsync.php';
require 'recipe/laravel.php';

inventory('hosts.yml');

set('rsync_src', __DIR__);
set('rsync_dest','{{release_path}}');

task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'rsync',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
//    'artisan:storage:link',
//    'artisan:view:clear',
//    'artisan:cache:clear',
//    'artisan:config:cache',
//    'artisan:optimize',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
])->desc('Deploy project');

//after('artisan:optimize', 'artisan:migrate');

after('deploy', 'success');